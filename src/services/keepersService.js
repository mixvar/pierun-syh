import philipImg from "@/assets/philip.jpg";
import harpsImg from "@/assets/harps.jpg";
import manandcatImg from "@/assets/manandcat.jpeg";
import michaelImg from "@/assets/michael.jpeg";

const MOCK_KEEPERS = [
  {
    id: "1",
    photo: philipImg,
    rating: 5,
    firstName: "Ania",
    location: "Krakow, Bronowice",
    superKeeper: true,
    description:
      "Cześć, jestem Ania. Interesuję się sportem oraz kuchnię włoskę. W wolnej chwili lubię aktywnie spędzać czas. Mam wieloletnie doświadczenie w behawariorystyce zwierząt. Kocham zwierzęta od dzieciństwa i jestem gotowa zaopkiekować się każdym pupilem.",
    opinions: [
      "Najlepsza oferta na rynku, polecam ~ Kasia właścielka kota Maćka",
      "Sprawdzona usługa, 100% gwarancja rzetelności i profesjonalizmu ~ Jan K.",
      "Polecam i zapraszam do współpracy ~ Jan N."
    ],
    price: 80
  },
  {
    id: "2",
    photo: manandcatImg,
    rating: 5,
    firstName: "Janusz",
    location: "Wieliczka",
    superKeeper: false,
    description:
      "Kocham koty, są one moim życiem. Chętnie zaopiekuje się twoim. Obiecuje że będzie się dobrze bawił z moją wesoła gromatką",
    opinions: [
      "Mój kot zaprzyjaźnił się z innymi. Chociaż myślałam że to nie możliwe - gorąco polecam Pana Janusza jako tymczasowego opiekuna ~ Kasia",
      "Mój kot nie chciał wracać do domu - zdecydowanie dużo lepsza opcja niż hotel dla zwierząt ~ Maria "
    ],
    price: 75
  },
  {
    id: "3",
    photo: harpsImg,
    rating: 3,
    firstName: "Adam",
    location: "Krakow, Nowa Huta",
    superKeeper: true,
    description:
      "Programista freelancer, miłośnik kawy. Miłośnik spacerów o każdej porze dnia. Chętnie zajmę się twoim psem i dostarczę mu potrzebną dawkę ruchu",
    opinions: ["Polecam tego reksiowicza ~ Adam"],
    price: 50
  },
  {
    id: "4",
    rating: 4,
    photo: michaelImg,
    firstName: "Mariola",
    location: "Krakow, Pradnik bialy",
    superKeeper: false,
    description:
      "Studentka medycyny, chętnie zadbam o twojego kota w czasie twojej nieobecności",
    opinions: ["Polecam ~ Annia właścielka Tosi i Mruczka"],
    price: 60
  }
];

function getNearbyKeepers(filters, limit) {
  return new Promise(resolve =>
    setTimeout(() => {
      resolve(MOCK_KEEPERS.slice(0, limit || MOCK_KEEPERS.length));
    }, 1000)
  );
}

function getKeeper(id) {
  return MOCK_KEEPERS.find(e => e.id === id);
}

export default {
  getNearbyKeepers,
  getKeeper
};
