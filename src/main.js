import Vue from "vue";
import "./plugins/vuetify";
import router from "./router";
import App from "./app/components/App.vue";
import firebase from "firebase";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import "./app/styles/global.scss";

Vue.config.productionTip = false;

const config = {
  apiKey: "AIzaSyCkMuZ_YvrYNfvOpbF-mufhzJ6ZCKdUdDM",
  authDomain: "reksio-f251e.firebaseapp.com",
  databaseURL: "https://reksio-f251e.firebaseio.com",
  projectId: "reksio-f251e",
  storageBucket: "reksio-f251e.appspot.com",
  messagingSenderId: "899236166543"
};
firebase.initializeApp(config);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
