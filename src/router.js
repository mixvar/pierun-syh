import Vue from "vue";
import Router from "vue-router";
import Home from "./app/components/Home.vue";
import CareTakersListPage from "./features/care-takers-list/CareTakersListPage";
import CareTakerDetailsView from "./features/care-taker-view/CareTakerDetailsView";
import UserTypePage from "./features/initial-form/UserTypePage";
import OwnerPetCareForm from "./features/initial-form/OwnerPetCareForm";
import CareTakerRegisterForm from "./features/initial-form/CareTakerRegisterForm";
import OwnerLoginPage from "./features/initial-form/OwnerLoginPage";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/form/user-type",
      component: UserTypePage
    },
    {
      path: "/form/owner",
      component: OwnerPetCareForm
    },
    {
      path: "/form/caretaker",
      component: CareTakerRegisterForm
    },
    {
      path: "/caretakers/list",
      component: CareTakersListPage
    },
    {
      path: "/caretakers/:id",
      component: CareTakerDetailsView
    },
    {
      path: "/login",
      component: OwnerLoginPage
    }
  ]
});
